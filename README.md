##Setup Develop Environment

###virtualenv

install

```bash
$ pip install virtualenv
```

init a virtual environment

```bash
$ virtualenv .venv
```

activate

```bash
$ source .venv/bin/activate
```

install dependencies

```bash
$ pip install -r requirements.txt
```

store dependencies

```bash
$ pip freeze > requirements.txt
```

###PyCharm

If using PyCharm, you may want to open the project from directory ./server, as there is the root of the python project.

Everything outside ./server and inside this directory is used for development environment setup.