from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf.urls import include, url
from views import index
import api


urlpatterns = [
    url(r'^$', index),
    url(r'^api/', include(api.urls)),
]

urlpatterns += staticfiles_urlpatterns()
