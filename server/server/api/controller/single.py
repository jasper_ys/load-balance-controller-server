import datetime


def balance_load(block):
    list = [{
        'sid': 'http://192.168.33.10:3000/task',
        'file': block,
        'offset': 1024 * (block % 10),
        'length': 1024,
        'timeStamp': datetime.datetime.now().time()
    },
    {
        'sid': 'http://192.168.33.11:3000/task',
        'file': block,
        'offset': 1024 * (block % 10),
        'length': 1024,
        'timeStamp': datetime.datetime.now().time()
    }]
    return list


