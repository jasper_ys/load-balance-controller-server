from django.views import generic
from urls import api, return_json
import controller

import urllib
import urllib2


@api
class Data(generic.View):

    url_regex = r'^data/$'

    @return_json
    def get(self, request):
        block = int(request.GET.get('block'))
        requests = controller.balance_load(block)

        # send data_req
        results = []
        for item in requests:
            content = Data.send_data_req(item)
            results.append(content)
        return results

    @staticmethod
    def send_data_req(info):
        data = urllib.urlencode(info)
        url = info['sid']
        req = urllib2.Request(url, data)
        result = urllib2.urlopen(req)
        content = result.read()
        return content
