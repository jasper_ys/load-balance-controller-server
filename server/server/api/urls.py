from django.conf.urls import url
from django.http import HttpResponse
import json


urlpatterns = []


def api(cls):
    p = url(cls.url_regex, cls.as_view())
    urlpatterns.append(p)
    return cls


def return_json(method):

    def decrator(self, request):
        r = method(self, request)
        return HttpResponse(json.dumps(r), content_type="application/json")

    return decrator
