import urllib2
from datetime import datetime

count = 0
now = datetime.now()
total = now - now

for x in xrange(1,1000):
    start = datetime.now()
    url = 'http://192.168.33.10:3000/task'
    req = urllib2.Request(url, 'timeStamp=00%3A05%3A27.414118&length=1024&offset=2048&file=2&sid=http%3A%2F%2F192.168.33.10%3A3000%2Ftask')
    result = urllib2.urlopen(req)
    content = result.read()
    end = datetime.now()
    count += 1
    total += (end - start)

print '%d requests %s ~ %s' % (count, str(total), str(total / count))