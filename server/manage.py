#!/usr/bin/env python
import os
import sys
import globals
import worker


globals.table.append('entry 0')
globals.table.append('entry 1')

worker.pawn_worker()

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "server.settings")

    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)
