import threading, time, sys, urllib2, os


def load_server_address(fname):
    with open(fname) as f:
        return f.readlines()


def run():
    while True:
        # status = request_server_status()
        for server in servers:
            print "worker (%s) fetch info from server %s" % (os.getpid(), server)
        time.sleep(10)
        # update table


def request_server_status():
    status = []
    for server in servers:
        content = urllib2.urlopen(server).read()
        status.append((server, content))
        print server + ": " + content
    return status


def pawn_worker():
    worker = threading.Thread(target=run)
    worker.start()
    print "worker (%s): %s" % (os.getpid(), servers)

started = False
servers = ['server_0', 'server_1', 'server_2']
